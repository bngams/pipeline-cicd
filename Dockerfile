# docker build -t docker-pipeline-cicd:latest .
# docker run -d -p 3000:3000 docker-pipeline-cicd
FROM alpine:3.19
RUN apk add --update && apk add nodejs npm && npm install -g mocha && npm install -g http-server
WORKDIR /app
COPY package*.json ./
COPY src ./src
EXPOSE 3000
CMD ["http-server", "-d", "-p", "3000", "src"]