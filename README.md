

# Créer un pipeline Ci Cd dans gitlab.

>Nous créerons un projet qui sera constitué d'une unique page html.
>Nous créerons le pipeline que nous utiliserons pour tester un module 
>JavaScript qui est une composante de la page html.
>Nous déploierons notre application dans un container docker.
>
>Le but premier de ce projet n'étant pas de faire une initiation au développement
>html et JavaScript. L'ensemble des sources sera fourni sans pour autant qu'elles 
>soient expliquées.
>
>Pour faciliter les manipulations git (add, commit, phush, pull) je fournis un 
>scripte bash, qui ne sera pas plus explicité. Son but est de rendre transparentes
>les commendes de base de git.


## Structure du systême de fichier du projet :

```
        .
        ├── Dockerfile
        ├── .dockerignore
        ├── .gitignore
        ├── git.sh
        ├── package.json
        ├── package-lock.json
        ├── README.md
        ├── src
        │   ├── index.html
        │   └── javascript
        │       ├── calculator.js
        │       └── index.js
        └── tests
            ├── tests2.js
            └── tests.js
```

### REQUIREMENTS

# git les commandes de bases et les bons usages.

>git est un logiciel de versionning de code à usage collaboratif. Il 
>travaille avec des branches. Ces branches peuvent être vues comme des 
>tiroirs. Dans chacune des branche il y aura une version du projet à 
>un niveau d'avancement différents.
>
>On ne doit pas travailler sur la branche de base de git.
>Cette branche contient le projet dans sa version finalisé.
>
>Il y a peu de commandes usuelle dans git et on peut les regrouper
>en quelques cas d'usage simple :
>
> (1) Initialiser un dépôt git.
>   On doit obligatoirement travailler dans un dossier vide.
>   On doit fournir à git un nom d'utilisateur et une adresse mail pour pouvoir 
>   travailler avec lui et pour pouvoir voir qui a fait quoi sur le projet.
>
>    **git config --global user.name  "Votre Nom"**
>
>    **git config --global user.email "Votre.Nom@mail.com"**
>
>   L'ensemble des données de la configuration de git sont dans le fichier 
>
>    **.git/config**
>
>   On peut afficher le contenu de ce fichier avec la commande.
>
>    **git config --list**
>
>   Il est possible d'initialiser un projet de deux façons, mais dans tous 
>   les cas, il faudra créer un dépôt git distant. Cela peut être sur des 
>   services tels que GitHub, GitLab, Bitbucket.
>
> (1.1) Initialiser un dépôt git local.
>
>    **git init**
>
>   On doit ensuite ajouter sa réplique distante.
>
>    **git remote add <NOM_LOCAL_REPO>  <URL_REPO>**
>
>   On doit ensuite lier la branche locale avec la branche distante.
>
>    **git branch --set-upstream-to=<NOM_LOCAL_REPO>/<NOM_BRANCHE>  <NOM_BRANCHE>**
>
>   Le premier NOM_BRANCHE est pour le branche distante,
>   le second est pour la branche locale.
>
> (1.2) Initialiser une réplique locale d'un projet distant
>
>    **git clone <URL_REPO>**
>
> Maintenant, que le dépôt est initialisé, nous avons deux cas d'usages possibles.
>
> (2) On travaille sur le dépôt local et on souhaite envoyer notre travail 
>     sur le dépôt distant. Ça se passe en trois étapes.
>
> (2.1) On ajoute tous les fichiers du projet au suivi de git.
>
>    **git add .**
>
> (2.2) On prend un cliché des fichiers qu'on a ajouté au suivi de git. 
>       Il est important de fournir un message qui explique pourquoi 
>       on prend ce cliché.
>
>    **git commit -m "Un message explicite pour ce commit."**
>
> (2.3) On envoie une copie de ce cliché sur le remote.
>
>    **git push**
>
> (3) On travaille sur le dépôt distant, on souhaite le rapatrier 
>     sur le dépôt local :
>
>    **pull**
>
> Voilà, on a fait le tour des commandes et cas d'usages de base.
> Il faut quand même ajouter deux trois autres commandes.
>
> La commande suivante permet de visualiser toutes les banches du projet.
> Elle permet également de voir la branche sur laquelle on se trouve (*), 
> et de voir si toutes les branches contiennent la même version du projet.
>
>    **git branch -a -v**
>
> Pour travailler sur le projet, il faut créer une branche de travail y 
> copier le contenue de la branche de base.  Solutions possibles pour 
> effectuer cela.
>
> Solution 1 :
>   Création de la nouvelle branche.
>
>    **git branch <NOM_NOUVELLE_BRANCHE>**
>
>   Se positionner sur la nouvelle branche.
>
>    **git checkout <NOM_NOUVELLE_BRANCHE>**
>
>   copier sur la nouvelle branche la branche distante de bas.
>
>    **git pull**
>
>  Solution 2 :
>   Création de la nouvelle branche et positionner cette branche.
>
>    **git checkout -b <NOM_NOUVELLE_BRANCHE>**
>
>   Copier sur la nouvelle branche la branche distante de bas.
>
>    **git pull**
>
> Ces commandes de bases sont évidemment à connaître et à comprendre, 
> mais le scripte bash git.sh se charge de ces commandes pour vous.
> Son usage est simple. :
>
>    **./git.sh**
>
> Il n'y a que deux options possibilités **PUSH** et **PULL**.


## NODEJS

> Dans ce programme, nous allons installer nodejs par ce que notre 
> page html contient du JavaScript. Nous installerons également la 
> dépendance mocha qui nous remettra de tester le module opération.js.
>
> Installer de node :
>
>    **npm install**
>
> Initialisationr de node :
>
>    **npm init -y**
>
> Installer de la dépendance pour les tests :
>
>    **npm install mocha**
>
> Après avoir installé mocha, 
> il va falloir paramètre le fichier package.json.
> Éditez ce fichier et remplacez les lignes :
> 
>    **"scripts": { "test": "mocha" },**
>
> par les lignes :
> 
>    **"scripts" : { "test" : "mocha ./tests/tests.js ./tests/tests2.js" },**
>
> Pour déclencher les tests localement, on utilise la commande suivante.
>
>    **npm test**


## docker

> Même si docker est un élément de notre programme,
> nous n'entrerons pas dans le détail du fonctionnement 
> et des commandes docker.
>
> Les deux commandes dont nos aurons besoin ont pour but de :
> Débrancher le container : 
>
>    **docker run -p 3000:3000 docker-pipeline-cicd:latest**
> 
> Liste les containers actifs :
>
>    **docker ps -a**
>
> Arrêter le container : 
>
>    **docker stop <CONTAINER_ID>**


## Contenu des fichiers du projet.


# index.html
```
<!DOCTYPE html>
<html lang = "fr">
  <head>
    <meta charset = "UTF-8">
    <meta name = "viewport" content = "width = device-width, initial-scale = 1.0">
    <script src = "./javascript/calculator.js"></script>
    <script src = "./javascript/index.js"></script>
    <noscript>Pour utiliser ce site, activez JavaScript.</noscript>
    <title>Calculatrice JavaScript</title>
  </head>
  <body>
  </body>
</html>
```

# index.js

```
document.addEventListener ("DOMContentLoaded", function () {

  function performOperation () {
    let value1 = document.getElementById ('nbr1').value
    let value2 = document.getElementById ('nbr2').value
    let result
    switch (document.getElementById ('operator').value) {
      case '+' : result = add (value1, value2) ; break
      case '-' : result = sub (value1, value2) ; break
      case '*' : result = mul (value1, value2) ; break
      case '/' : result = div (value1, value2) ; break
      default  : result = "Opération non prise en charge"
    }
    document.getElementById ('result').textContent = result
  }

  function init () {
    let nbr1 = Object.assign (document.createElement ('input'),
        { type : 'text', id : 'nbr1', placeholder : 'Donnée 1',
          style : 'width : 65px ; margin-right : 3px ;' })
    let operator = Object.assign (document.createElement ('input'),
        { type : 'text', id : 'operator', placeholder : 'Opération',
          style : 'width : 65px ; margin-right : 3px ;' })
    let nbr2 = Object.assign (document.createElement ('input'),
        { type : 'text', id : 'nbr2', placeholder : 'Donnée 2', 
          style : 'width : 65px ; margin-right : 3px ;' })
    let equals = Object.assign (document.createElement ('label'),
        { textContent : " = " })
    let result = Object.assign (document.createElement ('label'),
        { type : 'text', id : 'result' })
    let d1 = Object.assign (document.createElement ('div'),
        { id : 'd1', style : 'text-align : center ; white-space : nowrap ;' })
    let calculate = Object.assign (document.createElement ('button'),
        { textContent : "Calcul", onclick : performOperation })
    let d2 = Object.assign (document.createElement ('div'),
        { id : 'd2', style : 'text-align : center ; margin-top : 10px ;' })
    d1.appendChild (nbr1)
    d1.appendChild (operator)
    d1.appendChild (nbr2)
    d1.appendChild (equals)
    d1.appendChild (result)
    d2.appendChild (calculate)
    document.body.appendChild (d1)
    document.body.appendChild (d2)
    document.addEventListener ('keydown', function (event) { if (event.key === 'Enter')
     { calculate.click () } })
  }
  init ()
})
```

# calculator.js

```
function add (x, y) { return (+x) + (+y) }
function sub (x, y) { return (+x) - (+y) }
function mul (x, y) { return (+x) * (+y) }
function div (x, y) { return (+x) / (+y) }
module.exports = { add, sub, mul, div }
```

# test.js

```
const assert = require ('assert')
const { add, sub, mul, div } = require ('../src/javascript/calculator')
describe ('From test1 : test add', () => {
	it ('?? 20 +  20 = 40 ??', () => { assert.equal (add (20,  20), 40) })
 	it ('?? 20 + -20 =  0 ??', () => { assert.equal (add (20, -20),  0) })
})
describe ('From test1 : test sub', () => {
	it ('?? 20 -  20 =  0 ??', () => { require ('assert').equal (sub (20,  20),  0) })
 	it ('?? 20 - -20 = 40 ??', () => { require ('assert').equal (sub (20, -20), 40) })
})
```

# test2.js

```
const assert = require ('assert')
const { add, sub, mul, div } = require ('../src/javascript/calculator')
describe ('From test2 : test mul', () => {
	it ('?? 20 *  20 =  400 ??', () => { require ('assert').equal (mul (20,  20),  400) })
 	it ('?? 20 * -20 = -400 ??', () => { require ('assert').equal (mul (20, -20), -400) })
})
describe ('From test2 : test div', () => {
	it ('?? 20 / 20 =  1 ??',  () => { require ('assert').equal (div (20, 20),  1) })
	it ('?? 20 /  2 = 10 ??',  () => { require ('assert').equal (div (20,  2), 10) })
	it ('?? 20 /  0 = Nan ??', () => { require ('assert').equal (div (20,  0), "Infinity") })
})
```

# pipeline cicd.

> Le pipeline est dans le fichier **.gitlab-ci.yml**

``` 
cache :                                        # Spécifie les chemins des fichiers à mettre en cache.
  paths :
    - node_modules
image : node:21                                # Utilisation de l'image Docker Node.js version 21.
stages :                                       # Définition des étapes (stages) du pipeline.
  - build
  - test
  - deploy
# ---------------------------------------------
job_build :                                    # Job pour la construction de l'application.
  stage : build
  variables :
    begin_1 : "debut build"
    end_1 : "fin build"
  script :
    - echo "$begin_1"
    - npm install                              # Installation des dépendances npm.
    - npm install mocha                        # Installation de Mocha pour les tests.
    - echo "$end_1"
# ---------------------------------------------
job_test :                                     # Job pour l'exécution des tests.
  stage : test
  needs :
    - job : job_build
      artifacts : true
  variables :
    begin_2 : "debut test"
    end_2 : "fin test"
  script :
    - echo "$begin_2"
    - node_modules/.bin/mocha ./tests/tests.js # Exécution des tests avec Mocha.
    - echo "$end_2"
# ---------------------------------------------
job_deploy :                                   # Job pour le déploiement de l'application.
  stage : deploy
  needs :
    - job : job_test
      artifacts : true
  script :
    - echo "Building Docker image..."          # Construction de l'image Docker.
    - docker build -t docker-pipeline-cicd .   # Exécution de l'application dans un conteneur Docker.
    - docker run docker-pipeline-cicd node operations.js
  rules :
    - when : on_success                        # Déploiement est exécuté si le job de test est en succès.
``` 

# dockerfile

``` 
FROM node:alpine
# Définissez le répertoire de travail à l'intérieur du conteneur
WORKDIR /app
# Copiez package.json et package-lock.json dans le répertoire de travail
COPY package*.json ./
# Installez les dépendances && Installez le serveur HTTP
RUN npm install && npm install -g http-server
# Copiez uniquement le répertoire src de l'application
COPY src ./src
# Exposez le port sur lequel le serveur HTTP sera en cours d'exécution
EXPOSE 3000
# exécutera automatiquement la commande http-server pour 
# servir le contenu du répertoire src sur le port 3000.
CMD ["http-server", "-d", "-p", "3000", "src"]
``` 

``` 
 Voilà nous avons fait le tour de tout ce qui constitue le projet, le 
 pipeline ci cd et  le déploiement de notre application dans le container.
 ```