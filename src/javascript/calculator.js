
// calculator.js

function add (x, y) { return (+x) + (+y) }
function sub (x, y) { return (+x) - (+y) }
function mul (x, y) { return (+x) * (+y) }
function div (x, y) { return (+x) / (+y) }

// Exportez les fonctions pour pouvoir les utiliser dans les tests.
// if (typeof module !== 'undefined' && typeof module.exports !== 'undefined') 
  module.exports = { add, sub, mul, div }
// else
//   window.calculator = { add, sub, mul, div }
