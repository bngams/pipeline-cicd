
// index.js

document.addEventListener ("DOMContentLoaded", function () {

  function performOperation () {
    let value1 = document.getElementById ('nbr1').value
    let value2 = document.getElementById ('nbr2').value
    let result
    switch (document.getElementById ('operator').value) {
      case '+' : result = add (value1, value2) ; break
      case '-' : result = sub (value1, value2) ; break
      case '*' : result = mul (value1, value2) ; break
      case '/' : result = div (value1, value2) ; break
      default  : result = "Opération non prise en charge"
    }
    document.getElementById ('result').textContent = result
  }

  function init () {

    let nbr1 = Object.assign (document.createElement ('input'),
        { type : 'text', id : 'nbr1', placeholder : 'Donnée 1',
          style : 'width : 65px ; margin-right : 3px ;' })

    let operator  = Object.assign (document.createElement ('input'),
        { type : 'text', id : 'operator', placeholder : 'Opération',
          style : 'width : 65px ; margin-right : 3px ;' })

    let nbr2 = Object.assign (document.createElement ('input'),
        { type : 'text', id : 'nbr2', placeholder : 'Donnée 2', 
          style : 'width : 65px ; margin-right : 3px ;' })

    let equals = Object.assign (document.createElement ('label'),
        { textContent : " = " })

    let result = Object.assign (document.createElement ('label'),
        { type : 'text', id : 'result' })

    let d1 = Object.assign (document.createElement ('div'),
        { id : 'd1', style : 'text-align : center ; white-space : nowrap ;' })

    let calculate = Object.assign (document.createElement ('button'),
        { textContent : "Calcul", onclick : performOperation })

    let d2 = Object.assign (document.createElement ('div'),
        { id : 'd2', style : 'text-align : center ; margin-top : 10px ;' })

    d1.appendChild (nbr1)
    d1.appendChild (operator)
    d1.appendChild (nbr2)
    d1.appendChild (equals)
    d1.appendChild (result)
    d2.appendChild (calculate)

    document.body.appendChild (d1)
    document.body.appendChild (d2)

    document.addEventListener ('keydown', function (event) { if (event.key === 'Enter')
     { calculate.click () } })

  }
  init ()
})