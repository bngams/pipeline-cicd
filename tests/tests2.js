
// tests.js

// npm test

// Dans package.json remplacer le scripts test par ce qui suit : 
// "scripts": { "test" : "mocha ./tests/tests.js ./tests/tests2.js" },

const assert = require ('assert')

const { add, sub, mul, div } = require ('../src/javascript/calculator')

/*
  La fonction describe () regroupe tous les cas de test que notre code devrait valider.
    Elle prend deux arguments : 
      un nom pour le groupe de test, 
      une fonction de callback.

  La fonction it () identifie chaque cas individuel de tests unitaires. 
    Elle prend deux arguments : 
      une string décrivant ce que le test devrait faire, 
      une fonction de callback qui contient effectivement le code du test.
*/

/*
describe ('tFrom test2 : est add', () => {
	it ('?? 20 +  20 = 40 ??', () => { assert.equal (add (20,  20), 40) })
 	it ('?? 20 + -20 =  0 ??', () => { assert.equal (add (20, -20),  0) })
})

describe ('From test2 : test sub', () => {
	it ('?? 20 -  20 =  0 ??', () => { require ('assert').equal (sub (20,  20),  0) })
 	it ('?? 20 - -20 = 40 ??', () => { require ('assert').equal (sub (20, -20), 40) })
})
*/

describe ('From test2 : test mul', () => {
	it ('?? 20 *  20 =  400 ??', () => { require ('assert').equal (mul (20,  20),  400) })
 	it ('?? 20 * -20 = -400 ??', () => { require ('assert').equal (mul (20, -20), -400) })
})

describe ('From test2 : test div', () => {
	it ('?? 20 / 20 =  1 ??',  () => { require ('assert').equal (div (20, 20),  1) })
  it ('?? 20 /  2 = 10 ??',  () => { require ('assert').equal (div (20,  2), 10) })
  it ('?? 20 /  0 = Nan ??', () => { require ('assert').equal (div (20,  0), "Infinity") })
})
