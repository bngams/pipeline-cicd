#!/bin/bash

declare -g BRANCH_OF_WORK="dev"                                       # Branche de travail
declare -g BRANCH_ECHANGE="main"                                      # Branche pour pousser / tirrer sur le(s) remote(s).

check_branch () {                                                     # Vérifie la branche courrante.
  if [ ! "$(git branch --show-current)" == "$1" ] ; then
    if ! git show-ref --verify --quiet "refs/heads/$1" ; then         # Vérifier si la branche n'existe pas.
      git branch "$1"                                                 # Créer la branche.
    fi
    git checkout "$1"                                                 # On se positionne sur la branche.
  fi
}

read -p "Choisissez [1 PUSH, 2 PULL] : " choice

case $choice in
  1)
    echo "PUSH"
    check_branch "$BRANCH_OF_WORK"                                    # Verifie si on est bien sur la branche de travail.
    read -p "Message de commit : " commit_message                     # Méssage pour le commit.
    echo "$commit_message"
    git add .                                                         # Ajoute tous les fichier sur la branche de travail.
    git commit -m "$(date +"%Y-%m-%d %H:%M:%S") $commit_message"      # Fait le commit sur la branche de travail.
    # Il serait interressant de faire un pull remote/main -> main. a ce moment s'il y a un confil, on le vois en local...
    git checkout $BRANCH_ECHANGE                                      # Bascule sur la branche d'échange.
    git merge $BRANCH_OF_WORK                                         # Fusionne la branche de travail sur la branche d'échange.
    for REMOTE in $(awk -F"\"" '/\[remote/ {print $2}' ".git/config") # Recupère le(s) remote(s) de .git\confi.
      do git push $REMOTE $BRANCH_ECHANGE ; done                      # Effectuer le(s) push(s) sur le(s) remote(s).
    git checkout $BRANCH_OF_WORK                                      # Bascule sur la branch de travail.
    ;;
  2)
    echo "PULL"
    git branch   -D $BRANCH_ECHANGE                                   # Supprime la branche d'échange.
    git checkout -b $BRANCH_ECHANGE                                   # Créer la branche d'échange et on ce positionne dessus.
    check_branch   "$BRANCH_ECHANGE"                                  # Vérifie si on est bien sur la branche d'échange.
    git branch --set-upstream-to=gitlab/main main                     # Branche le main local sur le main remote.
    git pull                                                          # Réalisation du pull.
    git branch   -D $BRANCH_OF_WORK                                   # Supprime la branche de travail.
    git checkout -b $BRANCH_OF_WORK $BRANCH_ECHANGE                   # On crée la branche de travail à partir de la brange d'échange, on ce positionne dessus.
    check_branch   "$BRANCH_OF_WORK"                                  # Vérifie si on est bien sur la branche travail.
    ;;
  esac

git branch -a -v